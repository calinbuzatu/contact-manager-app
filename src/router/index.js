import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import appRoutes from './routes.json';

Vue.use(VueRouter);

const createRoute = route => {
  const { path, name, view } = route;

  return {
    name,
    path,
    component: () => import(/* webpackChunkName: "[request]" */ `@/views/${view}`),
  };
};

const routes = appRoutes.map(route => createRoute(route));

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const { name, params } = to;
  const { contactId } = params;

  // Router guard for incorrect contact id
  if (name === 'contact') {
    const contactData = store.getters['contacts/getContactById'](contactId);
    if (!contactData) {
      return next({ name: 'contact-404' });
    }
  }

  return next();
});

// Show progress for router loader
router.beforeResolve((to, from, next) => {
  store.dispatch('ui/toggleLoader');

  next();
});

router.afterEach(() => setTimeout(() => store.dispatch('ui/toggleLoader'), Vue.prototype.$config.routerDelay));

export default router;
