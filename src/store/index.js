import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
// Store modules
import ui from './ui';
import contacts from './contacts';

// Vuex Store modules
const modules = {
  ui,
  contacts,
};

// Store persistence for localStorage
const { storePersistencePrefix } = Vue.prototype.$config;
const vuex = new VuexPersist({
  key: storePersistencePrefix,
  storage: window.localStorage,
  modules: ['contacts'], // Persist only contacts module
});

Vue.use(Vuex);

export default new Vuex.Store({
  modules,
  plugins: [vuex.plugin],
});
