const initialState = {
  drawer: null,
  loaderDrawer: null,
  loader: null,
};

export default {
  namespaced: true,
  initialState,
  state: initialState,
  mutations: {
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_LOADER_DRAWER(state, payload) {
      state.loaderDrawer = payload;
    },
    SET_LOADER(state, payload) {
      state.loader = payload;
    },
  },
  actions: {
    toggleDrawer({ getters, commit }) {
      const { getDrawer } = getters;
      return commit('SET_DRAWER', !getDrawer);
    },
    toggleLoaderDrawer({ getters, commit }) {
      const { getLoaderDrawer } = getters;
      return commit('SET_LOADER_DRAWER', !getLoaderDrawer);
    },
    toggleLoader({ getters, commit }) {
      const { getLoader } = getters;
      return commit('SET_LOADER', !getLoader);
    },
  },
  getters: {
    getDrawer: state => state.drawer,
    getLoaderDrawer: state => state.loaderDrawer,
    getLoader: state => state.loader,
  },
};
