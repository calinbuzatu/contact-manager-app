const initialState = {
  contacts: [],
};

export default {
  namespaced: true,
  initialState,
  state: initialState,
  mutations: {
    SET_CONTACTS(state, payload) {
      state.contacts = payload;
    },
    REMOVE_CONTACT(state, payload) {
      state.contacts.splice(payload, 1);
    },
  },
  actions: {
    setContacts({ commit, getters }, payload) {
      const { getContactsLength } = getters;

      if (getContactsLength) { return; }

      commit('SET_CONTACTS', payload);
    },
    addContact({ commit, getters }, payload) {
      const { getContacts } = getters;
      const contacts = [
        ...getContacts,
        ...[payload],
      ];

      commit('SET_CONTACTS', contacts);
    },
    removeContact({ commit, getters }, payload) {
      const { getContactIndex } = getters;
      const index = getContactIndex(payload);

      commit('REMOVE_CONTACT', index);
    },
  },
  getters: {
    getContacts: state => state.contacts,
    getContactsLength: state => state.contacts.length,
    getContactById: (state, getters) => contactId => {
      const { getContacts } = getters;
      const contact = [...getContacts];
      return contact.find(c => c.id === contactId);
    },
    getContactsSortedAlphabetically: (state, getters) => {
      // Get contacts alphabetically by firstName and lastName property without any mutation side effect.
      // I've preferred to use the bubble sort algorithm after the technical interview talk.
      // Another alternative to make this sort was to use lodash utility library.
      const { getContacts } = getters;
      const sortedContacts = [...getContacts];
      const compare = (key, x, y) => x[key].localeCompare(y[key]);
      return sortedContacts.sort((a, b) => compare('firstName', a, b) || compare('lastName', a, b));
    },
    getContactsGroupedByLetter: (state, getters) => {
      const { getContactsSortedAlphabetically } = getters;
      const groupedContacts = [...getContactsSortedAlphabetically];
      /* eslint-disable no-param-reassign */
      return groupedContacts.reduce((accumulator, currentValue) => {
        const letterGroup = currentValue.firstName[0].toUpperCase();

        if (!accumulator[letterGroup]) {
          // New letter group
          accumulator[letterGroup] = [currentValue];
        } else {
          accumulator[letterGroup].push(currentValue);
        }

        return accumulator;
      }, {});
    },
    getContactIndex: state => contactId => state.contacts.findIndex(contact => contact.id === contactId),
  },
};
