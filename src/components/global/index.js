import InputSearch from './InputSearch';
import LoaderSpinner from './LoaderSpinner';

export default {
  InputSearch,
  LoaderSpinner,
};
