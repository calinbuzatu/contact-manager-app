import global from './global';
import layout from './layout';

export { global };
export { layout };

export default {
  global,
  layout,
};
