import LoaderProgress from './LoaderProgress';
import Drawer from './Drawer';
import Header from './Header';
import ContentRouter from './ContentRouter';

export default {
  LoaderProgress,
  Drawer,
  Header,
  ContentRouter,
};
