export default [
  {
    id: '72228468-01c7-11eb-adc1-0242ac120002',
    firstName: 'al',
    lastName: 'pacino',
    group: 'friends',
  },
  {
    id: '942bcb00-01c7-11eb-adc1-0242ac120002',
    firstName: 'alain',
    lastName: 'delon',
    group: 'friends',
  },
  {
    id: '90b05590-01c7-11eb-adc1-0242ac120002',
    firstName: 'anthony',
    lastName: 'hopkins',
    group: 'work',
  },
  {
    id: '8dca68f2-01c7-11eb-adc1-0242ac120002',
    firstName: 'brad',
    lastName: 'pitt',
    group: 'friends',
  },
  {
    id: '8b5f76f2-01c7-11eb-adc1-0242ac120002',
    firstName: 'joaquin',
    lastName: 'phoenix',
    group: 'work',
  },
  {
    id: '87c8bdc8-01c7-11eb-adc1-0242ac120002',
    firstName: 'matt',
    lastName: 'damon',
    group: 'friends',
  },
  {
    id: '224070e4-01c8-11eb-adc1-0242ac120002',
    firstName: 'michael',
    lastName: 'caine',
    group: 'others',
  },
];
