import contacts from './contacts';
import groups from './groups';

export default {
  contacts,
  groups,
};
