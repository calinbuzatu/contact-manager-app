import Vue from 'vue';

Vue.filter('uppercase', value => value.toUpperCase());
Vue.filter('capitalize', value => value.replace(/(^\w|\s\w)/g, m => m.toUpperCase()));
