// FrontEnd config file used to keep variables/parameters

// Node env variables
const isDev = process.env.NODE_ENV === 'development';

// Global component notation prefix convention for SuperBet
const globalComponentPrefix = 'SP';

const storePersistencePrefix = 'sp-store';

export default {
  isDev,
  globalComponentPrefix,
  storePersistencePrefix,
  theme: {
    primaryColor: 'red',
    elevation: 10,
  },
  routerDelay: 1000,
  requestDelay: 2000,
};
