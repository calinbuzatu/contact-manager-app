module.exports = {
  configureWebpack: {
    resolve: {
      extensions: ['.vue'],
    },
  },
  transpileDependencies: [
    'vuetify',
  ],
};
